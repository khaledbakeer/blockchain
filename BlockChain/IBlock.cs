using System;

namespace BlockChain
{
    public interface IBlock
    {
        #region Properties

        byte[] Data { get; }
        byte[] Hash { get; set; }
        int Nonce { get; set; }
        byte[] PrevHash { get; set; }
        DateTime TimeStamp { get; }

        #endregion
    }
}