using System;

namespace BlockChain
{
    public class Block : IBlock
    {
        #region Properties

        public byte[] Data { get; }
        public byte[] Hash { get; set; }
        public int Nonce { get; set; }
        public byte[] PrevHash { get; set; }
        public DateTime TimeStamp { get; }

        #endregion

        #region Constructor

        public Block(byte[] data)
        {
            Data = data ?? throw new ArgumentNullException(nameof(data));
            Nonce = 0;
            PrevHash = new byte[0x00];
            TimeStamp = DateTime.Now;
        }

        #endregion

        #region Public Methods

        public override string ToString()
        {
            return $"{BitConverter.ToString(Hash)} :\n {BitConverter.ToString(PrevHash)} \n {Nonce} {TimeStamp}";
        }

        #endregion
    }
}