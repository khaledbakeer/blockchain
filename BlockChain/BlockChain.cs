using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BlockChain
{
    public class BlockChain : IEnumerable<IBlock>
    {
        #region Properties

        public int Count => Items.Count;

        public List<IBlock> Items { get; set; } = new();

        public IBlock this[int index]
        {
            get => Items[index];
            set => Items[index] = value;
        }

        public byte[] Difficulty { get; }

        #endregion

        #region Constructor

        public BlockChain(byte[] difficulty, IBlock genesis)
        {
            Difficulty = difficulty;
            genesis.Hash = genesis.MineHash(difficulty);
            Items.Add(genesis);
        }

        #endregion

        #region Public Methods

        public void Add(IBlock item)
        {
            if (Items.LastOrDefault() != null) item.PrevHash = Items.LastOrDefault()?.Hash;

            item.Hash = item.MineHash(Difficulty);
            Items.Add(item);
        }

        public IEnumerator<IBlock> GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        #endregion

        #region Private Methods

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}