using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

namespace BlockChain
{
    public static class BlockChainExtension
    {
        #region Public Methods

        public static byte[] GenerateHash(this IBlock block)
        {
            using (SHA512 sha = new SHA512Managed())
            using (var memoryStream = new MemoryStream())
            using (var binaryWriter = new BinaryWriter(memoryStream))
            {
                binaryWriter.Write(block.Data);
                binaryWriter.Write(block.Nonce);
                binaryWriter.Write(block.TimeStamp.ToBinary());
                binaryWriter.Write(block.PrevHash);
                var memoryStreamArray = memoryStream.ToArray();
                return sha.ComputeHash(memoryStreamArray);
            }
        }

        public static byte[] MineHash(this IBlock block, byte[] difficulty)
        {
            if (difficulty is null) throw new ArgumentNullException(nameof(difficulty));
            var hash = new byte[0];
            var d = difficulty.Length;
            while (!hash.Take(2).SequenceEqual(difficulty))
            {
                block.Nonce++;
                hash = block.GenerateHash();
            }

            return hash;
        }

        public static bool IsValid(this IBlock block)
        {
            var bk = block.GenerateHash();
            return block.Hash.SequenceEqual(bk);
        }

        public static bool IsValidPrevBlock(this IBlock block, IBlock prevBlock)
        {
            if (prevBlock is null) throw new ArgumentNullException(nameof(prevBlock));
            var prev = prevBlock.GenerateHash();
            return prevBlock.IsValid() && block.PrevHash.SequenceEqual(prev); // GenerateHash twice TODO
        }

        public static bool IsValid(this IEnumerable<IBlock> items)
        {
            var enumerable = items.ToList();
            return enumerable.Zip(enumerable.Skip(1), Tuple.Create).All(block =>
                block.Item2.IsValid() && block.Item2.IsValidPrevBlock(block.Item1));
        }

        #endregion
    }
}