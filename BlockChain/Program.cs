﻿using System;
using System.Linq;

namespace BlockChain
{
    internal class Program
    {
        #region Private Methods

        private static void Main(string[] args)
        {
            var rnd = new Random(DateTime.Now.Millisecond);
            IBlock genesis = new Block(new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00 });
            byte[] difficulty = { 0x00, 0x00 };

            var blockChain = new BlockChain(difficulty, genesis);

            for (var i = 0; i < 100; i++)
            {
                var data = Enumerable.Range(0, 2256).Select(p => (byte)rnd.Next());
                blockChain.Add(new Block(data.ToArray()));

                Console.WriteLine(blockChain.LastOrDefault()?.ToString());
                Console.WriteLine($"Chain is Valid {blockChain.IsValid()}");
            }


            Console.WriteLine();
        }

        #endregion
    }
}